import React from 'react';
import { iterator } from '../library/iterator';
import style from './Cube.css';

const LENGTH = 9;

/**
 * Spinner type for the {@link Loader} component
 * @param {Object} props
 * @param {string} props.color
 * @return {ReactElement}
 */
export const Cube = ({ color }) => (
  <div className={style['cube']}>
    {iterator(LENGTH).map(item => (
      <div
        key={item}
        className={style['square']}
        style={{
          backgroundColor: color,
        }}
      />
    ))}
  </div>
);
