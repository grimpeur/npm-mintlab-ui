import { traverseMap } from '../../../library';

/**
 * @param {Object} parameters
 * @param {Object} parameters.theme
 * @param {string} parameters.error
 * @param {boolean} parameters.hasFocus
 * @return {JSS}
 */
export const getSelectStyles = ({
  theme: {
    palette: {
      common,
      primary,
      error: errorPalette,
      text,
    },
    typography: {
      fontFamily,
    },
    zIndex,
    mintlab: {
      greyscale,
      radius,
    },
  },
  error,
  hasFocus,
}) => {
  function backgroundColorGeneral() {
    return (error && !hasFocus) ? errorPalette.light : greyscale.light;
  }

  function backgroundColorOption(state) {
    const valueMap = new Map([
      [() => state.isSelected, () => greyscale.light],
      [() => state.isFocused, () => primary.light],
      [() => true, () => common.white],
    ]);
    return traverseMap(valueMap);
  }

  function borderBottom() {
    if (!error && hasFocus) {
      return `2px solid ${primary.main}`;
    }
  }

  return {
    container(base) {
      return {
        ...base,
        width: '100%',
        fontFamily,
      };
    },
    indicatorSeparator(base) {
      return {
        ...base,
        display: 'none',
      };
    },
    control(base) {
      return {
        ...base,
        boxShadow: 'none',
        border: 'none',
        borderBottom: borderBottom(),
        backgroundColor: backgroundColorGeneral(),
      };
    },
    valueContainer(base) {
      return {
        ...base,
        backgroundColor: backgroundColorGeneral(),
      };
    },
    option(base, state) {
      return {
        ...base,
        backgroundColor: backgroundColorOption(state),
        color: text.primary,
      };
    },
    multiValue(base) {
      return {
        ...base,
        borderRadius: radius.chip,
        padding: '2px',
      };
    },
    input(base) {
      return {
        ...base,
        input: {
          fontFamily,
        },
      };
    },
    multiValueRemove(base) {
      return {
        ...base,
        color: greyscale.darkest,
        backgroundColor: greyscale.darker,
        borderRadius: radius.chipRemove,
        transform: 'scale(0.85)',
      };
    },
    menu(base) {
      return {
        ...base,
        zIndex: zIndex.options,
      };
    },
  };
};

export default getSelectStyles;
