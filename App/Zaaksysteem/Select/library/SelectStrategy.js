import { createElement } from 'react';
import Select from './Select';
import CreatableSelect from './CreatableSelect';

/**
 * @param props
 * @param {boolean} props.creatable
 * @return {ReactElement}
 */
export const SelectStrategy = ({
  creatable = false,
  ...rest
}) => {
  if (creatable) {
    return createElement(CreatableSelect, rest);
  }

  return createElement(Select, rest);
};

export default SelectStrategy;
