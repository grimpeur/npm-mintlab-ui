import React, { Component } from 'react';
import classNames from 'classnames';
import MuiTextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import { bind, unique } from '../../library';
import { textFieldStyleSheet } from './TextField.style';

const DEFAULT_ROWS = 3;

/**
 * *Material Design* **Text field**.
 * - facade for *Material-UI* `TextField`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/TextField
 * @see /npm-mintlab-ui/documentation/consumer/manual/TextField.html
 * @see https://material-ui.com/api/text-field/
 *
 * @reactProps {Object} classes
 * @reactProps {Object} config
 * @reactProps {boolean} [disabled=false]
 * @reactProps {string} [error]
 * @reactProps {string} [info]
 * @reactProps {boolean} [required=false]
 * @reactProps {string} label
 * @reactProps {string} name
 * @reactProps {number} rows
 * @reactProps {string} value
 */
export class TextField extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      hasFocus: false,
    };

    bind(this, 'handleBlur', 'handleFocus');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactComponent}
   */
  render() {
    const {
      props: {
        classes,
        disabled = false,
        error,
        info,
        label,
        name,
        required = false,
        config,
        rows = DEFAULT_ROWS,
        value,
      },
      state: {
        hasFocus,
      },
      handleBlur,
      handleFocus,
      getStylingProps,
    } = this;

    return (
      <MuiTextField
        defaultValue={value}
        disabled={disabled}
        error={Boolean(error)}
        helperText={error || info}
        id={unique()}
        label={label}
        name={name}
        required={required}
        multiline={config && config.style === 'paragraph'}
        rows={rows}
        onBlur={handleBlur}
        onFocus={handleFocus}
        {...getStylingProps({
          classes,
          error,
          hasFocus,
        })}
      />
    );
  }

  // Custom methods:

  handleBlur() {
    this.setState({
      hasFocus: false,
    });
  }

  handleFocus() {
    this.setState({
      hasFocus: true,
    });
  }

  /**
   * @param {Object} parameters
   * @param {Object} parameters.classes
   * @param {string} parameters.error
   * @param {boolean} parameters.hasFocus
   * @return {Object}
   */
  getStylingProps({ classes, error, hasFocus }) {
    const {
      inputClass,
      inputErrorClass,
      formHelperErrorClass,
      inputLabelClass,
      inputLabelShrinkClass,
      inputLabelInactiveClass,
      inputLabelErrorClass,
      formControlClass,
    } = classes;

    const errorState = Boolean(error && !hasFocus);

    return {
      classes: {
        root: formControlClass,
      },
      InputProps: {
        classes: {
          input: classNames(inputClass, { [inputErrorClass]: errorState }),
        },
        disableUnderline: errorState,
      },
      FormHelperTextProps: {
        classes: {
          root: classNames({ [formHelperErrorClass]: errorState }),
        },
      },
      InputLabelProps: {
        classes: {
          root: classNames(inputLabelClass, inputLabelInactiveClass, { [inputLabelErrorClass]: errorState }),
          shrink: classNames(inputLabelClass, inputLabelShrinkClass, { [inputLabelErrorClass]: errorState }),
        },
      },
    };
  }
}

export default withStyles(textFieldStyleSheet)(TextField);
