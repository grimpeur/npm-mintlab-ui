import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import ReactSelect, { components } from 'react-select';
import ErrorLabel from '../../Form/library/ErrorLabel';
import Render from '../../../Abstract/Render';
import Icon from '../../../Material/Icon';
import { Body2 } from '../../../Material/Typography';
import { bind, callOrNothingAtAll, cloneWithout } from '../../../library';

import getSelectStyles from './reactSelect';
import materialUI from './materialUI';

export const DELAY = 400;
export const MIN_CHARACTERS = 3;
const MSG_CHOOSE = 'Select…';
const MSG_LOADING = 'Loading…';
const MSG_TYPE = 'Begin typing to search…';

/**
 * Choices and value must be provided in the form of a single, or array of objects.
 *
 * @typedef {Object} SelectValue
 * @property {string} value
 * @property {string} label
 * @property {Array<string>} alternativeLabels
 * @example
 * {
 *   value: 'strawberry',
 *   label: 'Strawberry',
 *   alternativeLabels: ['Fruit', 'Red'],
 * }
 */

/**
 * Facade for React Select v2.
 * - additional props are passed through to that component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Select
 * @see /npm-mintlab-ui/documentation/consumer/manual/Select.html
 * @see https://react-select.com/home
 *
 * @reactProps {boolean} [autoLoad=false]
 * @reactProps {SelectValue|Array<SelectValue>} choices
 * @reactProps {boolean} [disabled=false]
 * @reactProps {Function} getChoices
 * @reactProps {string} error
 * @reactProps {string} name
 * @reactProps {Function} onBlur
 * @reactProps {Function} onChange
 * @reactProps {Function} onFocus
 * @reactProps {Object} styles
 * @reactProps {Object} translations
 * @reactProps {SelectValue|Array<SelectValue>} value
 */
export class Select extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      translations: {
        'form:choose': MSG_CHOOSE,
        'form:loading': MSG_LOADING,
        'form:beginTyping': MSG_TYPE,
      },
      disabled: false,
      autoLoad: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    const { value } = props;

    super(props);
    this.state = {
      selected: value,
      isLoading: false,
      hasFocus: false,
    };
    this.timeoutId = null;
    bind(this,
      'handleBlur', 'handleChange', 'handleFocus', 'handleInputChange',
      'DropdownIndicator', 'ClearIndicator'
    );
  }

  // Lifecycle methods

  /**
   *  @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    if (this.props.autoLoad) {
      this.getChoices('');
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidupdate
   *
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    if (JSON.stringify(prevProps.choices) !== JSON.stringify(this.props.choices)) {
      this.setState({
        isLoading: false,
      });
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        choices: options,
        error,
        translations,
        theme,
        disabled,
        autoLoad,
        classes,
        styles,
        label,
        hasInitialChoices,
        ...rest
      },
      state: {
        selected,
        isLoading,
        hasFocus,
      },
      handleInputChange,
      handleFocus,
      handleChange,
      handleBlur,
      DropdownIndicator,
      ClearIndicator,
    } = this;

    return (
      <div>
        <Render condition={label}>
          <Body2
            classes={{
              body2: classes.label,
            }}
          >{label}</Body2>
        </Render>
        <ReactSelect
          isDisabled={disabled}
          cacheOptions={true}
          isLoading={(options && !options.length) ? false : isLoading}
          defaultValue={selected}
          options={options}
          loadingMessage={() => translations['form:loading']}
          noOptionsMessage={() => translations['form:beginTyping']}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onInputChange={!autoLoad && !hasInitialChoices && handleInputChange}
          placeholder={translations['form:choose']}
          styles={styles || getSelectStyles({
            theme,
            error,
            hasFocus,
          })}
          classNamePrefix='react-select'
          name={name}
          filterOption={this.filterOption}
          components={{
            DropdownIndicator,
            ClearIndicator,
          }}
          {...cloneWithout(rest, 'name', 'value', 'onBlur', 'onChange', 'onFocus')}
        />
        <Render
          condition={Boolean(error && hasFocus)}
        >
          <ErrorLabel label={error}/>
        </Render>
      </div>
    );
  }

  // Components

  /**
   * @param {Object} parentProps
   * @return {ReactElement}
   */
  DropdownIndicator(parentProps) {
    const { choices } = this.props;
    const { selectProps: { menuIsOpen } } = parentProps;

    const onClick = () => {
      if ((!choices && (Array.isArray(choices) && !choices.length)) ||
        !menuIsOpen
      ) {
        return;
      }

      this.getChoices('');
    };

    return (
      <div onClick={onClick}>
        <components.DropdownIndicator {...parentProps} >
          <Icon
            color='inherit'
          >arrow_drop_down</Icon>
        </components.DropdownIndicator>
      </div>
    );
  }

  /**
   * @param {Object} parentProps
   * @return {ReactElement}
   */
  ClearIndicator(parentProps) {
    const { clearIndicator } = this.props.classes;

    return (
      <components.ClearIndicator {...parentProps}>
        <Icon classes={{ root: clearIndicator }}>close</Icon>
      </components.ClearIndicator>
    );
  }

  // Custom methods

  /**
   * Call form change function, if provided.
   *
   * @param {Object} selected
   */
  handleChange(selected) {
    const {
      onChange,
      name,
    } = this.props;

    this.setState({
      selected,
    });

    callOrNothingAtAll(onChange, () => [{
      target: {
        name,
        value: selected,
        type: 'select',
      },
    }]);
  }

  /**
   * Call form blur function, if provided.
   *
   * @param {Event} event
   */
  handleBlur(event) {
    const {
      props: {
        onBlur,
        name,
      },
      state: { selected },
    } = this;

    event.stopPropagation();

    this.setState({
      hasFocus: false,
    });

    callOrNothingAtAll(onBlur, () => [{
      target: {
        name,
        value: selected,
        type: 'select',
      },
    }]);
  }

  /**
   * Call form focus function, if provided
   *
   * @param {Event} event
   */
  handleFocus(event) {
    const {
      props: {
        onFocus,
        name,
      },
    } = this;

    event.stopPropagation();

    this.setState({
      hasFocus: true,
    });

    callOrNothingAtAll(onFocus, () => [{
      target: {
        name,
      },
    }]);
  }

  /**
   * @param {string} input
   * @return {boolean}
   */
  isValidInput(input) {
    return Boolean(input) && (input.length >= MIN_CHARACTERS);
  }

  /**
   * @param {string} input
   */
  handleInputChange(input) {
    if (!this.isValidInput(input)) {
      return;
    }

    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }

    this.timeoutId = setTimeout(() => {
      this.getChoices(input);
    }, DELAY);
  }

  /**
   * @param {*} input
   */
  getChoices(input) {
    const { getChoices } = this.props;

    if (!getChoices) {
      return;
    }

    this.setState({
      isLoading: true,
    });

    getChoices(input);
  }

  /**
   * Custom filter function that also filters on alternative labels
   * @param {Object} option
   * @param {string} input
   * @return {boolean}
   */
  filterOption(option, input) {
    if (!input) {
      return true;
    }

    const { label, alternativeLabels } = option.data;
    const labels = [label]
      .concat(alternativeLabels)
      .filter(item => item);
    const clean = value =>
      value
        .trim()
        .toLowerCase();

    return labels.some(thisLabel =>
      clean(thisLabel).includes(clean(input))
    );
  }
}

export default withStyles(materialUI, {
  withTheme: true,
})(Select);
