# Known issues

- Inspecting a component with the React developer tools for
  Google Chrome by clicking on it does not work 
  (switching from the `Elements` pane to the `React` pane
  usually works, though). That seems to be related to 
  Storybook internals.

### (ZS-TODO: list as issues) FAQ

**Q: Why do I see "Warning: Failed prop type: The prop `className` is marked as required in `Resizer`, 
but its value is `undefined`." in the console?**

A: That's caused by an installed dependency. If it bugs you, find out which one and file an issue.

**Q: What *should* actually be published?**

A: With the excpetions of the `README`, `LICENSE` and `package.json` files,
nothing that's under version control (cf. `.gitignore` and `.npmignore`):

    - `distribution/ui.js` (`main` in `package.json`)
    - `distribution/ui.css`  

## Misc.

- https://github.com/babel/babel-eslint/issues/530
