import React from 'react';
import { mount, shallow } from 'enzyme';
import { Dialog } from '.';
import { fill } from '../../test';

/**
 * @test {Dialog}
 */
describe('The `Dialog` component', () => {
  test('renders nothing if its `open` prop is not truthy', () => {
    const wrapper = shallow(
      <Dialog
        title=""
        buttons={[]}
      />
    );
    const actual = wrapper.html();
    const expected = '';

    expect(actual).toBe(expected);
  });

  test('renders any number of buttons', () => {
    const count = 5;
    const wrapper = mount(
      <Dialog
        open={true}
        title=""
        buttons={fill(count, {
          text: '',
        })}
      />
    );
    const actual = wrapper
      .find('DialogActions ButtonBase')
      .length;
    const expected = count;

    expect(actual).toBe(expected);
  });
});
