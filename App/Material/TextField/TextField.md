# 🔌 `TextField` component

> *Material Design* **Text field**.

## See also

- [`TextField` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/TextField)
- [`TextField` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-textfield)

## External resources

- [*Material Guidelines*: Text fields](https://material.io/design/components/text-fields.html)
- [*Material-UI* `TextField` API](https://material-ui.com/api/text-field/) 
