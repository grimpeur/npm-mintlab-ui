import {
  React,
  stories,
  text,
  boolean,
} from '../../story';
import Select from '.';

const choices = [
  {
    value: 'chocolate',
    label: 'Chocolate',
    alternativeLabels: ['Yummy', 'I love this flavor'],
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
    alternativeLabels: ['Fruit', 'Red'],
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const choicesCreatable = [
  {
    value: 'chocolate',
    label: 'Chocolate',
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

stories(module, __dirname, {
  Default() {
    const [, defaultValue] = choices;

    return (
      <div
        style={{
          width: '15rem',
        }}
      >
        <Select
          choices={choices}
          error={text('Error', '')}
          isMulti={boolean('Multiple choices', false)}
          value={defaultValue}
          label={text('Label', '')}
        />
      </div>
    );
  },
  Creatable() {
    return (
      <Select
        creatable={true}
        error={text('Error', '')}
        value={choicesCreatable}
        label={text('Label', '')}
      />
    );
  },
});
