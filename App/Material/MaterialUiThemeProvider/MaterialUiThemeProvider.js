import { createElement } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { theme } from './library/theme';
import './library/roboto/roboto.css';

/**
 * Theme provider for wrapping the consumer's component tree root.
 *
 * @see /npm-mintlab-ui/documentation/consumer/manual/MaterialUiThemeProvider.html
 * @see https://material-ui.com/customization/themes/
 *
 * @param {Object} props
 * @param {*} props.children
 * @return {ReactElement}
 */
const MaterialUiThemeProvider = ({ children }) =>
  createElement(MuiThemeProvider, {
    children,
    theme,
  });

export default MaterialUiThemeProvider;
