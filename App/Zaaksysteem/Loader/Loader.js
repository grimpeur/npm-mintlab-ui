import { createElement } from 'react';
import { withTheme } from '@material-ui/core/styles';
import * as map from './Spinner';

/**
 * Loader component based on *SpinKit*.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Loader
 * @see /npm-mintlab-ui/documentation/consumer/manual/Loader.html
 *
 * @param {Object} props
 * @param {boolean} [props.active=true]
 * @param {ReactElement} props.children
 * @param {string} [props.color=props.theme.palette.common.black]
 * @param {Object} [props.theme]
 *   Material-UI theme
 * @param {string} [props.type='circle']
 *   Loader name; available loaders are
 *   - `'circle'` ({@link Circle})
 *   - `'cube'` ({@link Cube})
 *   - `'fold'` ({@link Fold})
 *   - `'pulse'` ({@link Pulse})
 *   - `'wave'` ({@link Wave})
 * @return {ReactElement}
 */
export const Loader = ({
  active = true,
  children,
  color,
  theme,
  type = 'circle',
}) => {
  if (active) {
    return createElement(map[type], {
      color: (color || theme.palette.common.black),
    });
  }

  return children;
};

export default withTheme()(Loader);
