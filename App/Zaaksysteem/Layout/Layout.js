import React from 'react';
import AppBar from './library/AppBar';
import PermanentDrawer from './library/PermanentDrawer/PermanentDrawer';
import TemporaryDrawer from './library/TemporaryDrawer/TemporaryDrawer';
import { layoutStyleSheet } from './Layout.style';
import { withStyles } from '@material-ui/core/styles';

const DEFAULT_INDEX = 0;
const DEFAULT_GUTTER = 0;

/**
 * Global Layout component.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Layout
 * @see /npm-mintlab-ui/documentation/consumer/manual/Layout.html
 *
 * @param {Object} props
 * @param {number} [props.active=0]
 * @param {*} props.children
 * @param {Object} props.classes
 * @param {string} props.customer
 * @param {Object} props.drawer
 * @param {Array} props.drawer.primary
 *    Array of Actions, (required).
 * @param {Array} props.drawer.secondary
 *    Array of Actions (optional).
 * @param {Object} props.drawer.about
 *    Single Action (optional).
 * @param {number} [props.gutter=0]
 *    Gutter for the content area. Only affects top, right and bottom.
 * @param {string} props.identity
 * @param {boolean} props.isDrawerOpen
 * @param {string} props.menuLabel
 * @param {Function} props.toggleDrawer
 * @param {string} props.userLabel
 * @param {Array} props.userActions
 *    Array of Actions.
 * @return {ReactElement}
 */
export const Layout = ({
  active = DEFAULT_INDEX,
  children,
  classes,
  customer,
  drawer,
  gutter = DEFAULT_GUTTER,
  identity,
  isDrawerOpen,
  menuLabel,
  toggleDrawer,
  userLabel,
  userName,
  userActions,
}) => (
  <div
    className={classes.root}
  >
    <AppBar
      className={classes.appBar}
      gutter={12}
      menuLabel={menuLabel}
      onMenuClick={toggleDrawer}
      position="absolute"
      userLabel={userLabel}
      userName={userName}
      userActions={userActions}
    >{identity}</AppBar>

    <PermanentDrawer
      active={active}
      className={classes.permanentDrawer}
      navigation={drawer.primary}
    />

    <TemporaryDrawer
      active={active}
      navigation={drawer}
      open={isDrawerOpen}
      subtitle={customer}
      title={identity}
      toggle={toggleDrawer}
    />

    <div
      className={classes.content}
      style={{
        padding: `${gutter}px ${gutter}px ${gutter}px 3px`,
      }}
    >{children}</div>
  </div>
);

export default withStyles(layoutStyleSheet)(Layout);
