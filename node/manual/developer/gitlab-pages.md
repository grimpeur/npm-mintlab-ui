# Gitlab pages

> Built with a the `pages` job in the ci pipeline.

[http://zaaksysteem.gitlab.io/npm-mintlab-ui/](http://zaaksysteem.gitlab.io/npm-mintlab-ui/)

Covers the generated HTML of

- [Storybook](https://storybook.js.org/)
    - Interactive demo and style guide
- [ESDoc](https://esdoc.org/)
    - API documentation and this manual :-) 
- [Jest](https://facebook.github.io/jest/)
    - Unit test coverage report
- [Webpack Visualizer](https://www.npmjs.com/package/webpack-visualizer-plugin)
    - Webpack bundle visualization
