import React, { Fragment } from 'react';
import Tooltip from '../../../Material/Tooltip/Tooltip';
import Render from '../../../Abstract/Render/Render';

/**
 * Wraps children with a tooltip if the condition is met
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {boolean} [props.condition=true]
 * @param {string} props.title
 * @param {string} props.type
 * @return {ReactElement}
 */
const TooltipWrapper = ({
  title,
  children,
  condition = true,
  type,
}) => (
  <Fragment>
    <Render
      condition={condition}
    >
      <Tooltip
        title={title}
        type={type}
      >{children}</Tooltip>
    </Render>
    <Render
      condition={!condition}
    >{children}</Render>
  </Fragment>
);

export default TooltipWrapper;
