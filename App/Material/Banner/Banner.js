import React from 'react';

/**
 * *Material Design* **Banner**.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Banner
 * @see /npm-mintlab-ui/documentation/consumer/manual/Banner.html
 *
 * @param {Object} props
 * @param {Array} props.children
 * @return {ReactElement}
 */
export const Banner = ({ children }) => (
  <div>
    {children}
  </div>
);

export default Banner;
