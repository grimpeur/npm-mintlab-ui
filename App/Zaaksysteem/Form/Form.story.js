import {
  React,
  stories,
  action,
  boolean,
} from '../../story';
import Form from '.';
import { Component } from 'react';

const { assign } = Object;
const DEFAULT_ERROR_MSG = 'Voer een geldige waarde in.';
const choices = [
  {
    value: 'chocolate',
    label: 'Chocolate',
  },
  {
    value: 'strawberry',
    label: 'Strawberry',
  },
  {
    value: 'vanilla',
    label: 'Vanilla',
  },
];

const fieldSets = [
  {
    title: 'Form story',
    description: 'Form story',
    fields: [
      {
        choices,
        constraints: ['required'],
        label: 'Select test',
        name: 'selecttest',
        promises: () => [
          import('react-select'),
        ],
        type: 'select',
        value: undefined,
      },
      {
        type: 'text',
        label: 'Test label 1',
        name: 'test1',
        isMulti: false,
        disabled: false,
        reference: '1',
        required: false,
        value: '',
        constraints: ['uri'],
      },
      {
        type: 'creatable',
        label: 'Creatable select',
        name: 'creatable',
        isMulti: true,
        disabled: false,
        promises: () => [
          import('react-select/lib/Creatable'),
          import('react-select'),
        ],
        required: false,
        value: choices,
        constraints: ['required'],
      },
    ],
  },
  {
    title: 'Form story',
    description: 'Form story',
    fields: [
      {
        constraints: ['required'],
        label: 'HTML editor',
        name: 'htmltest',
        type: 'html',
        value: 'test <strong>value</strong> in story',
      },
    ],
  },
];

const stores = {
  Default: {},
};

class FormStory extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      props: {
        mergedFieldSets,
        set,
        validate,
        identifier,
        changed,
      },
      state: {
        headerRef,
      },
    } = this;

    const setRef = header => {
      if (!this.state.headerRef) {
        this.setState({
          headerRef: header,
        });
      }
    };

    return (
      <div style={{
        width: '100%',
      }}>

        <header
          ref={setRef}
          style={{
            marginBottom: '30px',
            padding: '0px',
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'center',
          }}
        />

        <Form
          fieldSets={mergedFieldSets}
          save={action('SAVE')}
          set={set}
          validate={validate}
          changed={changed}
          statusTarget={headerRef}
          identifier={identifier}
        />
      </div>
    );
  }
}

stories(module, __dirname, {
  Default({ store }) {
    const set = keyValuePairs => {
      store.set(keyValuePairs);
    };

    // Merge the `Error(s)` knob's value into the fieldsets and
    // change the identifier to trigger the Form's `componentDidUpdate` after
    // changing the knob(s)

    const errors = boolean('Error(s)', false);
    const changed = boolean('Unsaved changes', false);
    const mergedFieldSets = fieldSets.map(fieldSet => assign({}, fieldSet, { errors }));
    const identifier = `FormStory-${errors}`;

    function validate() {
      return errors ? DEFAULT_ERROR_MSG : undefined;
    }

    return (
      <FormStory
        mergedFieldSets={mergedFieldSets}
        set={set}
        validate={validate}
        changed={changed}
        identifier={identifier}
      />
    );
  },
}, stores);
