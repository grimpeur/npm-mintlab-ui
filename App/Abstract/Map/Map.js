import { Fragment, createElement } from 'react';
import Clone from '../Clone';

/**
 * Declarative array to component mapper.
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Abstract/Map
 * @see /npm-mintlab-ui/documentation/consumer/manual/Map.html
 *
 * @param {Object} props
 * @param {Array} props.data
 * @param {Function} props.component
 * @return {React.Fragment}
 */
export const Map = ({
  data,
  component,
}) =>
  createElement(Fragment, {
    children:
      data
        .map((props, key) =>
          createElement(Clone, { key }, createElement(component, props))),
  });

export default Map;
