import React, { Component, Fragment } from 'react';
import Menu from '@material-ui/core/Menu';
import { DropdownMenuStylesheet } from './DropdownMenu.style';
import { bind, unique, cloneWithout } from '../../library';
import { withStyles } from '@material-ui/core/styles';

export { default as DropdownMenuList } from './library/DropdownMenuList';

/**
 * Dropdown menu component.
 * 
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/DropdownMenu
 * @see /npm-mintlab-ui/documentation/consumer/manual/DropdownMenu.html
 *
 * @reactProps {ReactElement} children
 *    The content to be shown in the popup window.
 * @reactProps {ReactElement} trigger
 *    The component that will serve as the trigger to activate the popup window.
 * @reactProps {Object} classes
 */
export class DropdownMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };
    bind(this, 'handleClick', 'handleClose');
  }

  // Lifecycle methods

  render() {
    const {
      handleClick,
      handleClose,
      props: {
        children,
        classes,
        trigger,
      },
      state: {
        anchorEl,
      },
    } = this;

    const open = Boolean(anchorEl);
    const id = unique();

    return (
      <Fragment>
        <span
          aria-owns={this.getOwner(open, id)}
          aria-haspopup="true"
          onClick={handleClick}
          role={this.getRole()}
        >{trigger}</span>
        <Menu
          classes={ cloneWithout(classes, 'list') }
          anchorEl={anchorEl}
          id={id}
          onClick={handleClose}
          open={open}
          MenuListProps={{
            classes: {
              root: classes.list,
            },
          }}
        >
          {children}
        </Menu>
        
      </Fragment>
    );
  }

  // Custom methods

  getOwner(open, id) {
    if (open) {
      return id;
    }

    return null;
  }

  isHtmlComponent() {
    const { type } = this.props.trigger;

    return typeof type === 'string';
  }

  isInteractive() {
    return this.props.trigger.type === 'button';
  }

  getRole() {
    if (this.isHtmlComponent() && !this.isInteractive()) {
      return 'button';
    }
  }

  /**
   * @param {Event} event
   * @param {Node} event.currentTarget
   */
  handleClick({ currentTarget }) {
    this.setState({
      anchorEl: currentTarget,
    });
  }

  handleClose() {
    this.setState({
      anchorEl: null,
    });
  }
}

export default withStyles(DropdownMenuStylesheet)(DropdownMenu);
