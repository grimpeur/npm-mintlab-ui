import { createMuiTheme } from '@material-ui/core/styles';

/**
 * Mintlab theme greyscale.
 *
 * @type {Object}
 */
const greyscale = {
  black: '#2A2E33',
  offBlack: '#2F3338',
  darkest: '#4E5764',
  evenDarker: '#AEB3B9',
  darker: '#D7D7DB',
  dark: '#F2F2F2',
  light: '#F7F7F8',
  lighter: '#FFFFFF',
};

/**
 * Mintlab theme color palette.
 *
 * Note that `review` and `support` are custom palette properties,
 * and `darker` and `lighter` are custom PaletteIntention properties.
 * They are provided with the palette for convenience, but have no
 * support in the Material-UI theme API.
 *
 * @see https://material-ui.com/customization/themes/#palette
 * @type {Object}
 */
export const palette = {
  primary: {
    darker: '#003F8B',
    dark: '#1D65CC',
    main: '#357DF4',
    light: '#C7E0FF',
    lighter: '#E3EEFF',
  },
  secondary: {
    darker: '#024B3A',
    dark: '#23AF6D',
    main: '#23D17F',
    light: '#74E2AE',
    lighter: '#D9FFD6',
  },
  error: {
    dark: '#C62443',
    main: '#FF345B',
    light: '#FBE9E7',
  },
  review: {
    dark: '#D18100',
    main: '#FF9D00',
    light: '#FFF5D9',
  },
  support: {
    main: '#004EE3',
  },
  common: {
    white: greyscale.lighter,
    black: greyscale.black,
  },
};

/**
 * Mintlab theme radii.
 *
 * @type {Object}
 */
export const radius = {
  button: 8,
  horizontalMenuButton: 30,
  notificationButton: 30,
  verticalMenuButton: 30,
  dialog: 10,
  sheet: 8,
  snackbar: 25,
  dropdownMenu: 15,
  dropdownButton: 8,
  chip: 12,
  chipRemove: '50%',
  wysiwyg: 8,
  textField: 8,
  toolTip: 8,
};

/**
 * Mintlab theme icon sizes.
 *
 * @type {Object}
 */
export const icon = {
  extraSmall: 18,
  small: 23,
  medium: 28,
  large: 36,
};

/**
 * Mintlab theme shadows.
 * 
 * @type {Object}
 */
export const shadows = {
  light: '3px 3px 10px 2px rgba(0,0,0,0.05)',
};

export const ICON_SELECTOR = '& > span:first-child > svg';

/**
 * Global overrides for Material-UI components.
 * This is preferable over using `withStyles` in the component facade.
 *
 * @type {Object}
 */
export const overrides = {
  MuiButton: {
    root: {
      borderRadius: radius.button,
      textTransform: 'none',
    },
    label: {
      // ZS-TODO: vertical alignment compensation; better ideas?
      paddingTop: '0.1rem',
      [ICON_SELECTOR]: {
        marginRight: '0.4rem',
      },
    },
    // Opting in for all variants would be very verbose
    // (see also ./CustomPalette.js).
    fab: {
      '& $label': {
        paddingTop: '0',
        [ICON_SELECTOR]: {
          marginRight: '0',
        },
      },
    },
    containedSecondary: {
      color: '#fff',
    },
  },
  MuiDialog: {
    paper: {
      borderRadius: radius.dialog,
    },
  },
  MuiDialogActions: {
    root: {
      '& > button': {
        marginRight: '12px',
      },
    },
  },
  MuiSnackbarContent: {
    root: {
      borderRadius: radius.snackbar,
      padding: '0 24px',
    },
  },
  MuiPaper: {
    root: {
      padding: '8px',
    },
  },
  MuiCard: {
    root: {
      overflow: 'visible',
      '& + $root': {
        marginTop: '25px',
      },
      boxShadow: shadows.light,
    },
  },
  MuiDivider: {
    root: {
      margin: '6px 0px',
    },
  },
};

/**
 * Material-UI theme.
 *
 * @type {Object}
 */
export const theme = createMuiTheme({
  overrides,
  palette,
  typography: {
    useNextVariants: true,
  },
  zIndex: {
    inputLabel: 100,
    statusBar: 1250,
    options: 1260,
  },
  mintlab: {
    greyscale,
    icon,
    radius,
    shadows,
  },
});
