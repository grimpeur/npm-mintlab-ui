import getSelectStyles from './reactSelect';

export const options = {
  theme: {
    palette: {
      common: {},
      primary: {},
      grey: {},
      text: {},
    },
    typography: {
      fontFamily: 'My Fancy Font Family',
    },
    zIndex: 1,
    mintlab: {},
  },
  error: 'My Tragic Error',
  hasFocus: true,
};

/**
 * @test {getSelectStyles}
 */
xdescribe('The `Select` component’s `getSelectStyles` factory function', () => {
  const api = getSelectStyles(options);

  describe('exports an object with', () => {
    describe('a `container` method', () => {
      test('that needs a better test', () => {
        const actual = api.container({
          foo: 'bar',
        });
        const expected = {
          foo: 'bar',
          fontFamily: 'My Fancy Font Family',
          width: '100%',
        };

        expect(actual).toEqual(expected);
      });
    });

    describe('an `indicatorSeparator` method', () => {
      test('that needs a better test', () => {
        const actual = api.indicatorSeparator({
          foo: 'bar',
        });
        const expected = {
          foo: 'bar',
          display: 'none',
        };

        expect(actual).toEqual(expected);
      });
    });

    describe('a `control` method', () => {
      test('that needs a better test', () => {
        const actual = api.control();
        const expected = {
          backgroundColor: undefined,
          border: 'none',
          borderBottom: undefined,
          boxShadow: 'none',
        };

        expect(actual).toEqual(expected);
      });
    });

    describe('a `valueContainer` method', () => {
      test('that needs a better test', () => {
        const actual = api.valueContainer();
        const expected = {};

        expect(actual).toEqual(expected);
      });
    });

    describe('an `option` method', () => {
      test('that needs a better test', () => {
        const actual = api.option({}, {});
        const expected = {};

        expect(actual).toEqual(expected);
      });
    });

    describe('a `multiValue` method', () => {
      test('that needs a better test', () => {
        const actual = api.multiValue();
        const expected = {
          borderRadius: '12px',
          padding: '2px',
        };

        expect(actual).toEqual(expected);
      });
    });

    describe('an `input` method', () => {
      test('that needs a better test', () => {
        const actual = api.input();
        const expected = {
          input: {
            fontFamily: 'My Fancy Font Family',
          },
        };

        expect(actual).toEqual(expected);
      });
    });

    describe('a `multiValueRemove` method', () => {
      test('that needs a better test', () => {
        const actual = api.multiValueRemove();
        const expected = {
          backgroundColor: undefined,
          borderRadius: '50%',
          color: undefined,
          transform: 'scale(0.85)',
        };

        expect(actual).toEqual(expected);
      });
    });

    describe('a `menu` method', () => {
      test('that needs a better test', () => {
        const actual = api.menu();
        const expected = {};

        expect(actual).toEqual(expected);
      });
    });
  });
});
