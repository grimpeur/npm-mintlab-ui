import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import { components } from 'react-select';
import ReactSelectCreatable from 'react-select/lib/Creatable';
import ErrorLabel from '../../Form/library/ErrorLabel';
import Render from '../../../Abstract/Render';
import Icon from '../../../Material/Icon/Icon';
import { Body2 } from '../../../Material/Typography';
import { bind, callOrNothingAtAll, cloneWithout } from '../../../library';

import getSelectStyles from './reactSelect';
import materialUI from './materialUI';

const MSG_CREATABLE = 'Typ, en <ENTER> om te bevestigen.';
const MSG_CREATE = 'Aanmaken:';

/**
 * Facade for Creatable React Select v2.
 * - additional props will be passed to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/CreatableSelect
 * @see /npm-mintlab-ui/documentation/consumer/manual/CreatableSelect.html
 * @see https://react-select.com/home
 *
 * @reactProps {string} error
 * @reactProps {string} name
 * @reactProps {Function} onBlur
 * @reactProps {Function} onChange
 * @reactProps {Object} translations
 * @reactProps {Object|Array} value
 * @reactProps {Object} styles
 * @reactProps {Object} components
 */

export class CreatableSelect extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      translations: {
        'form:creatable': MSG_CREATABLE,
        'form:create': MSG_CREATE,
      },
      disabled: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    const { value } = props;

    this.state = {
      value,
      hasFocus: false,
    };

    bind(this, 'handleBlur', 'handleChange', 'handleFocus', 'ClearIndicator');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        error,
        translations,
        theme,
        styles,
        disabled,
        classes,
        label,
        ...rest
      },
      state: {
        value: currentValue,
        hasFocus,
      },
      handleChange,
      handleBlur,
      handleFocus,
      ClearIndicator,
    } = this;

    const createLabel = input =>
      [translations['form:create'], ' ', input].join('');

    return (
      <div>
        <Render condition={label}>
          <Body2
            classes={{ body2: classes.label }}>
            {label}
          </Body2>
        </Render>
        <ReactSelectCreatable
          isDisabled={disabled}
          isMulti={true}
          components={{
            DropdownIndicator: null,
            ClearIndicator,
          }}
          defaultValue={currentValue}
          onChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          placeholder={translations['form:creatable']}
          formatCreateLabel={createLabel}
          noOptionsMessage={() => translations['form:creatable']}
          styles={styles || getSelectStyles({
            theme,
            error,
            hasFocus,
          })}
          {...cloneWithout(rest, 'components', 'config', 'name', 'value', 'onBlur', 'onChange', 'onFocus')}
        />
        <Render
          condition={Boolean(error && hasFocus)}
        >
          <ErrorLabel
            label={error}
          />
        </Render>
      </div>
    );
  }

  // Custom methods:

  /**
   * Call form change function, if provided
   * @param {*} value
   */
  handleChange(value) {
    const { onChange, name } = this.props;

    this.setState({
      value,
    });

    callOrNothingAtAll(onChange, () => [{
      target: {
        name,
        value,
        type: 'creatable',
      },
    }]);
  }

  /**
   * Call form blur function, if provided
   * @param {Event} event
   */
  handleBlur(event) {
    const {
      props: { onBlur, name },
      state: { value },
    } = this;

    event.stopPropagation();

    this.setState({
      hasFocus: false,
    });

    callOrNothingAtAll(onBlur, () => [{
      target: {
        name,
        value,
        type: 'creatable',
      },
    }]);
  }

  // Components

  /**
   * Call form focus function, if provided
   * @param {Event} event
   */
  handleFocus(event) {
    const { props: { onFocus, name } } = this;
    event.stopPropagation();

    this.setState({
      hasFocus: true,
    });

    callOrNothingAtAll(onFocus, () => [{
      target: {
        name,
      },
    }]);
  }

  /**
   * @param {*} parentProps
   * @return {ReactElement}
   */
  ClearIndicator(parentProps) {
    const { clearIndicator } = this.props.classes;

    return (
      <components.ClearIndicator {...parentProps}>
        <Icon classes={{ root: clearIndicator }}>close</Icon>
      </components.ClearIndicator>
    );
  }
}

export default withStyles(materialUI, {
  withTheme: true,
})(CreatableSelect);
