import React, { Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import Render from '../../../Abstract/Render';
import ErrorLabel from '../../Form/library/ErrorLabel';
import { bind, callOrNothingAtAll } from '../../../library';
import { wysiwygStyleSheet } from './Wysiwyg.style';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

/**
 * Rich text editor facade for *React Draft Wysiwyg*.
 * - additional props are passed through to that component
 *
 * Dependencies {@link Render}
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Wysiwyg
 * @see /npm-mintlab-ui/documentation/consumer/manual/Wysiwyg.html
 * @see https://jpuri.github.io/react-draft-wysiwyg/#/docs
 *
 * @reactProps {boolean} disabled
 * @reactProps {string} [error]
 * @reactProps {string} name
 * @reactProps {Function} [onChange]
 * @reactProps {Function} [onBlur]
 * @reactProps {string} [value='']
 */
export class Wysiwyg extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      toolbar: {
        options: ['inline'],
      },
      value: '',
      disabled: false,
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);

    const contentBlock = htmlToDraft(props.value);
    this.state = {
      hasFocus: false,
    };

    if (contentBlock) {
      const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
      const editorState = EditorState.createWithContent(contentState);

      this.state.editorState = editorState;
    }

    bind(this, 'handleBlur', 'handleChange', 'handleFocus');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      props: {
        error,
        classes: {
          defaultClass,
          errorClass,
          editorClass,
        },
        disabled,
        ...rest
      },
      state: {
        editorState,
        hasFocus,
      },
      handleBlur,
      handleChange,
      handleFocus,
    } = this;

    return (
      <div>
        <Editor
          defaultEditorState={editorState}
          onEditorStateChange={handleChange}
          onBlur={handleBlur}
          onFocus={handleFocus}
          wrapperClassName={classNames(
            defaultClass, {
              [errorClass]: error,
            })}
          toolbarClassName={classNames(
            defaultClass, {
              [errorClass]: error,
            })}
          editorClassName={classNames(
            defaultClass,
            editorClass, {
              [errorClass]: error,
            })}
          readOnly={disabled}
          {...rest}
        />
        <Render condition={Boolean(hasFocus && error)}>
          <ErrorLabel label={error}/>
        </Render>
      </div>
    );
  }

  // Custom methods:

  /**
   * @param {name} name
   * @param {Object} editorState
   * @return {Object}
   */
  getEditorValue(name, editorState) {
    const value = draftToHtml(convertToRaw(editorState.getCurrentContent()));

    return [{
      target: {
        name,
        type: 'html',
        value,
      },
    }];
  }

  /**
   * @param {Object} editorState
   */
  handleChange(editorState) {
    const { name, onChange } = this.props;

    this.setState({
      editorState,
    });

    callOrNothingAtAll(onChange, () => this.getEditorValue(name, editorState));
  }

  /**
   * @param {Event} event
   */
  handleBlur(event) {
    const { onBlur, name } = this.props;

    event.stopPropagation();

    this.setState({
      hasFocus: false,
    });

    callOrNothingAtAll(onBlur, () => this.getEditorValue(name, this.state.editorState));
  }

  /**
   * @param {Event} event
   */
  handleFocus(event) {
    const { onFocus, name } = this.props;

    event.stopPropagation();

    this.setState({
      hasFocus: true,
    });

    callOrNothingAtAll(onFocus, () => [{
      target: {
        name,
      },
    }]);
  }
}

export default withStyles(wysiwygStyleSheet)(Wysiwyg);
