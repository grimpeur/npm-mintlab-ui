import React from 'react';
import MuiCard from '@material-ui/core/Card';
import MuiCardContent from '@material-ui/core/CardContent';
import MuiCardHeader from '@material-ui/core/CardHeader';
import Render from '../../Abstract/Render';

/**
 * *Material Design* **Card**.
 * - facade for *Material-UI* `Card`, `CardHeader` and `CardContent`
 * - children are rendered in the `CardContent`,
 *   additional props are passed through
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Card
 * @see /npm-mintlab-ui/documentation/consumer/manual/Card.html
 *
 * @param {Object} props
 * @param {string} props.description
 * @param {string} props.title
 * @return {ReactElement}
 */
const Card = props => {
  const {
    classes,
    contentClasses,
    children,
    title,
    description,
    ...rest
  } = props;

  return (
    <MuiCard
      classes={classes}
      {...rest}
    >
      <Render condition={title}>
        <MuiCardHeader
          title={title}
          subheader={description}
        />
      </Render>

      <MuiCardContent
        classes={contentClasses}
      >
        { children }
      </MuiCardContent>
    </MuiCard>
  );
};

export default Card;
