import {
  React,
  stories,
  text,
} from '../../story';
import TextField from '.';

stories(module, __dirname, {
  Default() {
    return (
      <TextField
        info={text('Info', 'Type ahead')}
        label={text('Label', 'Text field')}
        error={text('Error', '')}
        required={true}
      />
    );
  },
});
