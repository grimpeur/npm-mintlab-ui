import React from 'react';
import Button from '../../Button';

export const DialogAction = ({
  onClick,
  presets,
  text,
}) => (
  <Button
    action={onClick}
    presets={presets}
  >{text}</Button>
);
