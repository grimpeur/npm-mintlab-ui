# 🔌 `RadioGroup` component

> *Material Design* **Radio button** selection control.

## See also

- [`RadioGroup` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/RadioGroup)
- [`RadioGroup` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-radiogroup)

## External resources

- [*Material Guidelines*: Radio buttons](https://material.io/design/components/selection-controls.html#radio-buttons)
- [*Material-UI* `Radio` API](https://material-ui.com/api/radio/)
- [*Material-UI* `RadioGroup` API](https://material-ui.com/api/radio-group/)
