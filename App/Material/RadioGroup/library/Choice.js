import React from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

/**
 * Facade for *Material-UI* `FormControlLabel`.
 *
 * @param {Object} props
 * @param {boolean} [props.disabled=false]
 * @param {string} props.label
 * @param {string} props.value
 * @param {number} index
 * @return {ReactElement}
 */
export const Choice = ({
  disabled = false,
  label,
  value,
}, index) => (
  <FormControlLabel
    key={index}
    control={<Radio/>}
    disabled={disabled}
    label={label}
    value={value}
  />
);
