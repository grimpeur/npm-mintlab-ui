const {
  cwd,
  env: {
    npm_package_config_PUBLIC,
  },
} = process;
const rootDir = cwd();

module.exports = {
  cacheDirectory: '/tmp',
  coverageDirectory: `./${npm_package_config_PUBLIC}/coverage`,
  coveragePathIgnorePatterns: [
    '<rootDir>/node/',
  ],
  coverageReporters: [
    "json",
    "lcov",
  ],
  moduleNameMapper: {
    '\\.(css|scss)$': '<rootDir>/node/test/double/dummy/style.js'
  },
  rootDir,
  setupFiles: [
    '<rootDir>/node/test/polyfill.js',
    '<rootDir>/node/test/enzyme.js'
  ],
  testMatch: [
    '<rootDir>/**/*.test.js',
  ],
};
