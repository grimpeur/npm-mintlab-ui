import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=H2
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const H2 = ({
  children,
  classes,
}) => (
  <Typography
    variant="h2"
    classes={classes}
  >{children}</Typography>
);
