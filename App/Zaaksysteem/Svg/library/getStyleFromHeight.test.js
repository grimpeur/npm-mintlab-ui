import { getStyleFromHeight } from './getStyleFromHeight';

const WIDTH = 100;
const HEIGHT = 50;
const viewbox = [WIDTH, HEIGHT];

describe('The `Svg` component’s `getStyleFromHeight` utility function', () => {
  test('returns', () => {
    const actual = getStyleFromHeight(viewbox, '100px');
    const asserted = {
      width: '200px',
      height: '100px',
    };

    expect(actual).toEqual(asserted);
  });

  test('adds a left padding instead of height if the height is a percentage', () => {
    const actual = getStyleFromHeight(viewbox, '80%');
    const asserted = {
      height: '80%',
      paddingLeft: '80%',
    };

    expect(actual).toEqual(asserted);
  });
});
