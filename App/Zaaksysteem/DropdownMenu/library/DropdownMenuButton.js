import React, { Fragment } from 'react';
import MuiButton from '@material-ui/core/Button';
import Icon from '../../../Material/Icon';
import { dropdownMenuButtonStylesheet } from './DropdownMenuButton.style';
import { withStyles } from '@material-ui/core/styles';
import { cloneWithout } from '../../../library';

/**
 * @param {Object} props
 * @param {Function} props.action
 * @param {string} props.icon
 * @param {string} props.label
 * @param {Object} props.classes
 * @return {ReactElement}
 */
export const DropdownMenuButton = ({
  action,
  icon,
  label,
  classes,
}) => {
  const children = icon ? (
    <Fragment>
      <Icon
        size='small'
      >{icon}</Icon>
      {label}
    </Fragment>
  ) : label;

  return (
    <MuiButton
      onClick={action}
      classes={cloneWithout(classes, 'wrapper')}
      fullWidth={true}
    >{children}</MuiButton>
  );
};

export default withStyles(dropdownMenuButtonStylesheet)(DropdownMenuButton);
