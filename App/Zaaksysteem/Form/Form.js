import React, { Component, createElement, Fragment } from 'react';
import { createPortal } from 'react-dom';
import { withStyles } from '@material-ui/core/styles';
import Render from '../../Abstract/Render';
import Card from '../../Material/Card';
import * as typeMap from './library/typemap';
import TooltipWrapper from './library/TooltipWrapper';
import InfoIcon from './library/InfoIcon';
import StatusBar from './library/StatusBar';
import { bind, equals } from '../../library';
import { formStyleSheet } from './Form.style';

const { assign, keys } = Object;

/**
 * Composite Form component.
 *
 * Depends on {@link Card} and {@link Render}
 *
 * @see  /npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Form
 * @see /npm-mintlab-ui/documentation/consumer/manual/Form.html
 *
 * @reactProps {Array<Object>} fieldSets
 *   Form control configurations, grouped, with an optional title and description
 * @reactProps {Function} save
 *   Save the external state manager's model values
 * @reactProps {Function} set
 *   Set a form control's model value in the external state manager
 * @reactProps {Function} validate
 *   Validate a form control value literal
 * @reactProps {string} identifier
 *   String used to generate a unique key for every form field
 * @reactProps {DOMNode} statusTarget
 *   DOM Node in which to render the StatusBar component
 * @reactProps {boolean} changed
 *   Whether there are any unsaved changes
 */
export class Form extends Component {
  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = this.getStateFromFields();
    this.setExternalState();
    bind(this, 'onBlur', 'onFocus', 'onChange');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidupdate
   *
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    if (
      !equals(prevProps.identifier, this.props.identifier) &&
      !equals(prevProps.fieldSets, this.props.fieldSets)
    ) {
      this.setState(this.getStateFromFields());
      this.setExternalState();
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      getFieldSet,
      onBlur,
      onFocus,
      onChange,
      props: {
        classes,
        fieldSets,
        save,
        changed,
        statusTarget,
      },
      state: {
        disabled,
      },
    } = this;

    return (
      <Fragment>
        {statusTarget && createPortal(
          <StatusBar
            save={save}
            disabled={disabled}
            changed={changed}
          />, statusTarget)}

        <div
          onBlur={onBlur}
          onChange={onChange}
          onFocus={onFocus}
          className={classes.form}
        >
          {fieldSets && fieldSets.map(getFieldSet, this)}
        </div>
      </Fragment>
    );
  }

  // Custom methods:

  /**
   * Called when an onChange event happens in a form field.
   *
   * This method is also passed as a prop to every form field component.
   * Form components that do not emit an onChange event natively may call
   * this method to implement their own onChange triggering.
   *
   * @param {Object} event
   * @param {Object} event.target
   * @param {boolean} event.target.checked
   * @param {string} event.target.name
   * @param {string} event.target.type
   * @param {string} event.target.value
   */
  onChange({
    target: {
      checked,
      name,
      type,
      value,
    },
  }) {
    const field = this.getFieldByName(name);

    if (!field) return;

    this.processFormField({
      field,
      name,
      checked,
      type,
      value,
    });
  }

  /**
   * Called whenever a form component loses focus.
   *
   * This method is also passed as a prop to every form field component.
   * Form components that do not emit an onBlur event natively may call
   * this method to implement their own onBlur triggering.
   */
  onBlur() {
    this.setState({
      focus: undefined,
    });
  }

  /**
   * Called whenever a form component gains focus.
   *
   * This method is also passed as a prop to every form field component.
   * Form components that do not emit an onFocus event natively may call
   * this method to implement their own onFocus triggering.
   * @param {Object} event
   * @param {Object} event.target
   * @param {string} event.target.name
   */
  onFocus({ target: { name } }) {
    this.setState({
      focus: name,
    });
  }

  /**
   * @param {Object} fieldSet
   * @param {*} index
   * @return {ReactElement}
   */
  getFieldSet({
    title,
    description,
    fields,
  }, index) {
    const {
      props: {
        identifier,
      },
      getFormControl,
    } = this;

    return (
      <Fragment
        key={`${identifier}-${index}`}
      >
        <Render condition={title && fields.length}>
          <Card
            title={title}
            description={description}
          >
            {fields.map(getFormControl, this)}
          </Card>
        </Render>

        <Render condition={!title}>
          {fields.map(getFormControl, this)}
        </Render>
      </Fragment>
    );
  }

  /**
   * @param {Object} props
   * @param {string} [props.constraints=[]]
   * @param {string} props.name
   * @param {string} props.type
   * @param {string} [props.value]
   * @return {ReactElement}
   */
  getFormControl({
    constraints = [],
    name,
    type,
    value,
    ...rest
  }) {
    const {
      state: {
        errors,
        focus,
      },
      props: {
        identifier,
        classes,
      },
      onChange,
      onBlur,
      onFocus,
    } = this;

    const error = errors[name];
    const required = constraints.includes('required');
    const hasFocus = focus === name;

    // ZS-TODO: connect infoIcon to translations
    // ZS-TODO: only render if there is a tooltip for this field
    return (
      <div
        key={`${identifier}-${name}`}
        className={classes.formRow}
      >

        <TooltipWrapper
          condition={Boolean(error && !hasFocus)}
          title={error}
        >
          {createElement(typeMap[type], {
            // ZS-FIXME: add generic normalization
            defaultChecked: (typeof value === 'boolean') ? value : undefined,
            error,
            name,
            onFocus,
            onBlur,
            onChange,
            required,
            value,
            ...rest,
          })}
        </TooltipWrapper>

        <Render condition={true}>
          <TooltipWrapper
            title={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi pellentesque aliquam magna eu ullamcorper. '}
            type='info'
          >
            <InfoIcon/>
          </TooltipWrapper>
        </Render>

      </div>
    );
  }

  /**
   * Get a flattened array of all fields in the fieldset
   *
   * @param {Object} fieldSet
   * @return {Array}
   */
  getFieldList(fieldSet) {
    if (fieldSet) {
      const reduceFields = (accumulator, { fields }) =>
        accumulator.concat(fields);

      return fieldSet.reduce(reduceFields, []);
    }

    return [];
  }

  /**
   * Get the formatted literal value if it is valid. Otherwise, `undefined`.
   *
   * @param {string} name
   * @param {*} value
   * @return {string|undefined}
   */
  getModelValue(name, value) {
    const { errors } = this.state;

    if (errors.hasOwnProperty(name)) {
      return;
    }

    return value;
  }

  /**
   * @param {string} needle
   * @return {Object}
   */
  getFieldByName(needle) {
    const { fieldSets } = this.props;
    const findField = ({ name }) => name === needle;

    return this
      .getFieldList(fieldSets)
      .find(findField);
  }

  /**
   * @param {Object} options
   * @param {boolean} options.checked
   * @param {string} options.type
   * @param {string} options.value
   * @param {Array<string>} options.constraints
   * @return {boolean|number|string}
   */
  getNormalizedFormValue({
    checked,
    type,
    value,
    constraints,
  }) {
    if (type === 'checkbox') {
      return checked;
    }

    if (constraints && constraints.includes('number')) {
      return Number(value);
    }

    return value;
  }

  /**
   * Process a field's onChange event.
   *
   * Validate the (processed) value against the field's constraints. Then, set
   * the form's internal state accordingly. As long as any form error remains,
   * submission is disabled. Then, call the external state manager with the new
   * key/value pair.
   *
   * @param {Object} parameters
   * @param {string} parameters.field
   * @param {string} parameters.name
   * @param {boolean} parameters.checked
   * @param {string} parameters.type
   * @param {*} parameters.value
   */
  processFormField({
    field,
    name,
    checked,
    type,
    value,
  }) {
    const {
      constraints,
      config,
    } = field;
    const {
      props: {
        validate,
        set,
      },
      state: {
        errors,
      },
    } = this;

    /**
     * Update the form's internal state, based on the field's errors.
     *
     * @param {string|undefined} error
     */
    const updateInternalState = error => {
      const nextErrors = assign({}, errors);

      if (error) {
        nextErrors[name] = error;
      } else {
        delete nextErrors[name];
      }

      this.setState({
        disabled: this.isDisabled(nextErrors),
        errors: nextErrors,
      });
    };

    /**
     * Delegate this key/value pair to the external state manager.
     * If the actual value is not valid, it is set to `undefined`.
     *
     * @param {*} value
     * @param {string|undefined} error
     */
    const updateExternalState = (value, error) => {
      if (error) {
        set({
          [name]: undefined,
        });
      } else {
        const nextValue = this.getModelValue(name, value);
        // ZS-FIXME: current only match is resetting boolean/checkbox
        // needs back-end input
        set({
          [name]: nextValue,
        });
      }
    };

    const normalizedValue = this.getNormalizedFormValue({
      checked,
      type,
      value,
      constraints,
      config,
    });

    const error = validate(normalizedValue, constraints, config);

    updateInternalState(error);
    updateExternalState(normalizedValue, error);
  }

  /**
   * Get the form's submission state.
   * It is either enabled without errors or disabled with one or more errors.
   *
   * @return {Object}
   */
  getStateFromFields() {
    const { fieldSets, validate } = this.props;

    const reduceErrors = (accumulator, { constraints, name, value, config }) => {
      const error = validate(value, constraints, config);

      if (error) {
        accumulator[name] = error;
      }

      return accumulator;
    };

    const errors = this
      .getFieldList(fieldSets)
      .reduce(reduceErrors, {});

    return {
      disabled: this.isDisabled(errors),
      errors,
    };
  }

  /**
   * Save all key/value pairs to the external state manager.
   */
  setExternalState() {
    const { fieldSets, set } = this.props;
    const values = this
      .getFieldList(fieldSets)
      .reduce((accumulator, { name, value }) =>
        assign(accumulator, {
          [name]: this.getModelValue(name, value),
        }), {});

    set(values);
  }

  /**
   * @param {Object} errors
   * @return {boolean}
   */
  isDisabled(errors) {
    return Boolean(keys(errors).length);
  }
}

export default withStyles(formStyleSheet)(Form);
