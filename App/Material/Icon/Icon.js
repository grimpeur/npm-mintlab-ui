import { createElement } from 'react';
import { withTheme } from '@material-ui/core/styles';
import * as iconMap from './library';

/**
 * *Material Design* **Icon**.
 * - facade for a subset of `@material-ui/icons`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Icon
 * @see /npm-mintlab-ui/documentation/consumer/manual/Icon.html
 * @see https://material-ui.com/api/icon/
 *
 * @param {Object} props
 * @param {string} props.children
 * @param {Object} [props.classes]
 * @param {string} [props.color]
 * @param {string} [props.size=medium]
 * @return {ReactElement}
 */
export const Icon = ({
  children,
  classes,
  color,
  size = 'medium',
  theme: {
    mintlab: {
      icon,
    },
  },
}) =>
  createElement('span', {
    style: {
      display: 'inline-flex',
      fontSize: `${icon[size]}px`,
    },
  }, createElement(iconMap[children], {
    classes,
    color,
    fontSize: 'inherit',
  }));

export default withTheme()(Icon);
