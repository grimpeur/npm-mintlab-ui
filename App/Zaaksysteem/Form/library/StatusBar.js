import React from 'react';
import { Body1 } from '../../../Material/Typography';
import { withStyles } from '@material-ui/core/styles';
import { statusBarStyleSheet } from './StatusBar.style';
import Button from '../../../Material/Button/Button';
import Paper from '../../../Material/Paper/Paper';
import classNames from 'classnames';
import Render from '../../../Abstract/Render/Render';

const MSG_ERRORS = 'Er zitten fouten in één of meerdere configuratie-items.';
const MSG_UNSAVED = 'Er zijn onopgeslagen configuratie-items.';
const MSG_SAVE = 'Opslaan';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} props.save
 * @param {boolean} [props.disabled=false]
 * One or more errors are present
 * @param {boolean} [props.changed=false]
 * Form value(s) have been changed, but not yet saved
 * @param {Object} props.translations
 * @return {ReactElement}
 */
const StatusBar = ({
  classes,
  save,
  disabled = false,
  changed = false,
  translations = {
    ['form:errorsInForm']: MSG_ERRORS,
    ['form:unsavedChanges']: MSG_UNSAVED,
    ['form:save']: MSG_SAVE,
  },
}) => (
  <Render condition={disabled || changed}>
    <Paper
      classes={{
        root: classNames(
          classes.bar,
          { [classes.disabled]: disabled }
        ),
      }}
    >
      <Render condition={disabled}>
        <Body1
          classes={{
            root: classNames(classes.text, classes.error),
          }}
        >{translations['form:errorsInForm']}</Body1>
      </Render>

      <Render condition={!disabled && changed}>
        <Body1
          classes={{
            root: classNames(classes.text, classes.info),
          }}
        >{translations['form:unsavedChanges']}</Body1>
      </Render>

      <Button
        disabled={disabled}
        action={save}
        presets={['contained', 'primary']}
      >
        {translations['form:save']}
      </Button>
    </Paper>
  </Render>
);

export default withStyles(statusBarStyleSheet)(StatusBar);
