import {
  React,
  stories,
  text,
} from '../../story';
import Banner from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Banner>
        {text('Greeting', 'Hello, world!')}
      </Banner>
    );
  },
});
