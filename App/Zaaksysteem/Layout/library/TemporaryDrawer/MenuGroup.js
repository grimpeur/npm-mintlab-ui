import React from 'react';
import classNames from 'classnames';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Icon from '../../../../Material/Icon/Icon';
import { Body2 } from '../../../../Material/Typography/Typography';
import { menuGroupStylesheet } from './MenuGroup.style';
import { withStyles } from '@material-ui/core/styles';

/**
 * @param {Array} navigation
 * @param {Object} classes
 * @param {number} active
 */
export const MenuGroup = ({
  navigation,
  classes,
  active,
}) => (
  <MenuList
    classes={{
      root: classes.menuList,
    }}
  >
    {navigation.map(({
      action,
      icon,
      label,
    }, index) => {
      const isActive = (active === index);

      return (
        <MenuItem
          key={index}
          data-index={index}
          classes={{
            root: classNames(classes.root, {
              [classes.active]: isActive,
            }),
          }}
          onClick={() => action()}
        >
          <ListItemIcon
            classes={{
              root: classes.inherit,
            }}
          >
            <Icon
              size='small'
              classes={{
                root: classes.inherit,
              }}
            >{icon}</Icon>
          </ListItemIcon>
          <ListItemText
            disableTypography={true}
          >
            <Body2 classes={{
              root: classes.inherit,
            }}>{label}</Body2>
          </ListItemText>
        </MenuItem>
      );
    })}
  </MenuList>
);

export default withStyles(menuGroupStylesheet)(MenuGroup);
