/**
 * Style Sheet for the {@link Button} component custom `semiContained` variant.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const buttonSemiContained = ({
  palette: {
    common,
    primary,
    secondary,
  },
}) => ({
  contained: {
    color: common.black,
    background: 'transparent',
    '&:hover': {
      background: 'transparent',
    },
  },
  containedPrimary: {
    color: primary.main,
    background: 'transparent',
  },
  containedSecondary: {
    color: secondary.main,
    background: 'transparent',
  },
});
