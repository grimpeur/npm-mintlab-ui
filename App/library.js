const { assign, keys } = Object;
const { stringify } = JSON;

// ZS-FIXME: this occasionally fails
/**
 * Generate a unique identifier.
 *
 * @private
 * @param {string} [prefix=zs]
 * @return {string}
 */
export const unique = (prefix = 'zs') =>
  `${prefix}-${window.performance.now()}`;

/**
 * Get a shallow clone of an object without the specified properties.
 *
 * @private
 * @param {Object} object
 * @param {Array} exclusions
 * @return {Object}
 */
export const cloneWithout = (object, ...exclusions) =>
  keys(object)
    .reduce((accumulator, key) => {
      if (!exclusions.includes(key)) {
        accumulator[key] = object[key];
      }

      return accumulator;
    }, {});

/**
 * Bind an infinite set of methods to a class instance.
 *
 * @private
 * @param {Object} instance
 *   Instance of a class
 * @param {...string} methods
 *   Method names of a class
 */
export function bind(instance, ...methods) {
  const reduceBoundMethods = (accumulator, name) =>
    assign(accumulator, {
      [name]: instance[name].bind(instance),
    });
  const boundMethods = methods.reduce(reduceBoundMethods, {});

  assign(instance, boundMethods);
}

/**
 * Deeply compare values that can be serialized as JSON.
 * Note that the order of object properties is NOT normalized.
 *
 * @private
 * @param {*} leftHandSide
 * @param {*} rightHandSide
 * @return {boolean}
 */
export const equals = (leftHandSide, rightHandSide) =>
  stringify(leftHandSide) === stringify(rightHandSide);

/**
 * Traverse the map, and return the output of the
 * first value function where the key function is truthy
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map
 *
 * @param {Map} map
 * @param {Array} [keyArgs=[]]
 * @param {Array} [functionArgs=[]]
 */
export const traverseMap = (map, keyArgs = [], functionArgs = []) => {
  for (const [mapKey, mapFunction] of map) {
    if (mapKey(...keyArgs)) {
      return mapFunction(...functionArgs);
    }
  }
};

/**
 * Call a value with optional arguments if it is a function.
 *
 * @param {*} callback
 *   A value that is called if it is a function.
 * @param {Function} getArguments
 *   The optional arguments for the callback.
 * @returns {*}
 *   The return value of the function if it is called or undefined.
 */
export function callOrNothingAtAll(callback, getArguments = () => []) {
  if (typeof callback === 'function') {
    return callback(...getArguments());
  }
}
