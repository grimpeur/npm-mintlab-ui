# 🔌 `Layout` component

> Global Layout component.

## See also

- [`Layout` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Layout)
- [`Layout` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-layout)
