// @see https://material-ui.com/api/button/
/**
 * @type {Array<string>}
 */
export const colors = ['default', 'inherit', 'primary', 'secondary', 'review', 'danger'];
/**
 * @type {Array<string>}
 */
export const sizes = ['small', 'medium', 'large'];
/**
 * @type {Array<string>}
 */
export const iconSizes = ['extraSmall', 'small', 'medium', 'large'];
/**
 * @type {Array<string>}
 */
export const variants = ['fab', 'text', 'icon', 'outlined', 'contained', 'semiContained'];

/**
 * Get a mutually exlcusive array intersection value.
 *
 * @param {Array<string>} haystack
 * @param {Array<string>} needles
 * @return {string|undefined}
 */
export function oneOf(haystack, needles) {
  const [result, overflow] = needles
    .filter(name => haystack.includes(name));

  if (result && !overflow) {
    return result;
  }
}

export function getSizes(presets) {
  if (presets.includes('icon')) {
    return iconSizes;
  }

  return sizes;
}

/**
 * @param {Array} presets
 * @return {Object}
 */
export const parsePresets = presets => ({
  color: oneOf(colors, presets),
  fullWidth: presets.includes('fullWidth'),
  mini: presets.includes('mini'),
  size: oneOf(getSizes(presets), presets),
  variant: oneOf(variants, presets),
});

/**
 * @param {*} value
 * @param {string} type
 * @return {*}
 */
function getByType(value, type) {
  if (typeof value == type) {
    return value;
  }
}

/**
 * Get an object with mutually exclusive `href` and `onClick` properties.
 *
 * @param {Function|string} action
 * @return {Object}
 */
export const parseAction = action => ({
  href: getByType(action, 'string'),
  onClick: getByType(action, 'function'),
});
