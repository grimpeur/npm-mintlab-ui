import React from 'react';
import { mount } from 'enzyme';
import { Layout } from '.';
import { fill } from '../../test';

jest.mock('../../Material/Icon', () => 'span');

const classes = {
  root: '',
};

/**
 * @test {Layout}
 */
xdescribe('The `Layout` component', () => {
  test('is a composite component for `AppBar` and `Drawer`', () => {
    const wrapper = mount(
      <Layout
        classes={classes}
        drawer={[]}
      />
    );
    const actual = [
      wrapper.exists('AppBar'),
      wrapper.exists('Drawer'),
    ];
    const asserted = [true, true];

    expect(actual).toEqual(asserted);
  });

  test('renders an `CompactButton` for each `drawer` prop item', () => {
    const count = 4;
    const wrapper = mount(
      <Layout
        classes={classes}
        drawer={fill(count, {
          icon: 'add',
        })}
      />
    );
    const actual = wrapper
      .find('nav > ul CompactButton')
      .length;
    const asserted = count;

    expect(actual).toEqual(asserted);
  });

  test('calls a `drawer` prop action with its array index', () => {
    const spy = jest.fn();
    const count = 3;
    const ONE = 1;
    const wrapper = mount(
      <Layout
        classes={classes}
        drawer={fill(count, {
          action: spy,
          icon: 'add',
        })}
      />
    );
    const lastIconLink = wrapper
      .find('nav > ul CompactButton')
      .last();
    const assertedTimesCalled = 1;

    lastIconLink.simulate('click');

    expect(spy).toHaveBeenCalledTimes(assertedTimesCalled);
    expect(spy).toHaveBeenCalledWith(count - ONE);
  });
});
