# CLI commands

In your host, run

    $ docker-compose exec app sh

## Start the storybook

    $ npm start

## Generate a component

    $ npm run plop

- Provide the *component name* and choose a 
  *component type* when prompted.

## Run the tests

    $ npm test

to run the tests once or
    
    $ npm run tdd

to watch the source code for changes during development.

## Lint the JS files

    $ npm run lint

## Generate this documentation

    $ npm run docs
