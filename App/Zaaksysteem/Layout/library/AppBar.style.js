/**
 * Style Sheet for the AppBar component
 * @return {JSS}
 */
export const appBarStyleSheet = ({
  breakpoints,
  mintlab: {
    shadows,
  },
}) => ({
  appBar: {
    boxShadow: shadows.light,
  },
  content: {
    flexGrow: 1,
    padding: '0 14px',
  },
  userName: {
    padding: '8px 8px 4px 8px',
  },
  toolbar: {
    [breakpoints.up('xs')]: {
      minHeight: '48px',
    },
  },
});
