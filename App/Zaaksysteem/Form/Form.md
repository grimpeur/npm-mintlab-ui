# 🔌 `Form` component

> Composite Form component.

## Abstract (`ZS-FIXME`: out of date)

- render **uncontrolled** fields from `prop`
    - on construction and on new fieldSets prop
        - validate all field values
        - save all valid values to external store
- event delegation
    - on `input` and `blur`
        - validate the `target` value
        - disable/enable the form
    - on `blur`
        - save valid target value to external store
- submit
    - send external store values to permanent data storage
    
## See also

- [`Form` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Form)
- [`Form` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-form)
   