/**
 * @param {Object} theme
 * @return {JSS}
 */
export const formStyleSheet = ({
  breakpoints,
}) => ({
  form: {
    width: '100%',
  },
  formRow: {
    display: 'grid',
    'grid-template-columns': 'auto 30px',
    'grid-column-gap': '20px',
    'align-items': 'center',
    [breakpoints.up('md')]: {
      'grid-template-columns': '50% 30px',
    },
    '& + $formRow': {
      marginTop: '28px',
    },
  },
});
