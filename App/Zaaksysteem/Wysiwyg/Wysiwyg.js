import { createElement } from 'react';
import LazyLoader from '../../Abstract/LazyLoader';

export const Wysiwyg = props =>
  createElement(LazyLoader, {
    promise: () => import(
      // https://webpack.js.org/api/module-methods/#import
      /* webpackChunkName: "ui.wysiwyg" */
      './library/Wysiwyg'),
    ...props,
  });

export default Wysiwyg;
