/**
 * Style Sheet for the {@link VerticalMenu} component.
 * @return {JSS}
 */
export const VerticalMenuStylesheet = () => ({
  menu: {
    display: 'flex',
    'align-items': 'baseline',
    'flex-direction': 'column',
    '& >*:not(:last-child)': {
      marginBottom: '34px',
    },
  },
});
