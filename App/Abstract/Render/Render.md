# 🔌 `Render` component

> Use a *JSX* component instead of (yet) an(other) expression 
  to conditionally render a component.

## Example

### Expression 

    <div>
      {condition && <SomeComponent/>}
    </div>

### `Render` component 

    <div>
      <Render condition={true}>
        <SomeComponent/>
      </Render>
    </div>

## See also

- [`Render` stories](/npm-mintlab-ui/storybook/?selectedKind=Abstract/Render)
- [`Render` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#abstract-render)
