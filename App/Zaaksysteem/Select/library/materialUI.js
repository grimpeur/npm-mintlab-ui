const materialUi = () => ({
  clearIndicator: {
    fontSize: '16px',
  },
  label: {
    marginBottom: '8px',
  },
});

export default materialUi;
