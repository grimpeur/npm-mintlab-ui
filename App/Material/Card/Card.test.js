import React from 'react';
import { mount } from 'enzyme';
import Card from '.';

/**
 * @test {Card}
 */
describe('The `Card` component', () => {
  test('renders the title prop in a `CardHeader` component', () => {
    const wrapper = mount(
      <Card
        title="Hello!"
      >
        <div>Hello, World!</div>
      </Card>
    );
    const actual = wrapper.find('Render CardHeader').length;
    const asserted = 1;

    expect(actual).toBe(asserted);
  });

  test('does not render the `CardHeader` component when there is no title prop', () => {
    const wrapper = mount(
      <Card>
        <div>Hello, World!</div>
      </Card>
    );
    const actual = wrapper.find('Render CardHeader').length;
    const asserted = 0;

    expect(actual).toBe(asserted);
  });
});
