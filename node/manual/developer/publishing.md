# Publishing to the npm registry

When a merge request is merged to the protected master,
a new version is automatically published.

See  `/.gitlab-ci.yml`.

TODO: commit message format documentation
