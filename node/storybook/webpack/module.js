const cherrypack = require('@mintlab/cherrypack');
const { join } = require('path');
const { assign } = Object;

const APP_INCLUDEPATHS = [
  join(process.cwd(), 'App'),
];
const NODE_MODULES_INCLUDEPATHS = /node_modules/;

const STYLE_LOADER = 'style-loader';
const CSS_LOADER = {
  loader: 'css-loader',
  options: {
    importLoaders: 1,
    localIdentName: '[name]__[local]___[hash:base64:5]',
    modules: true,
    sourceMap: true,
  },
};
const POSTCSS_LOADER = {
  loader: 'postcss-loader',
  options: {
    plugins:[
      require('postcss-import')(),
      require('postcss-cssnext')(),
    ],
    sourceMap: true,
  },
};

const baseRules = [
  {
    // App
    test: /\.css$/,
    include: APP_INCLUDEPATHS,
    use: [STYLE_LOADER, CSS_LOADER, POSTCSS_LOADER],
  },
  {
    // Node modules
    test: /\.css$/,
    include: NODE_MODULES_INCLUDEPATHS,
    use: [
      STYLE_LOADER,
      assign({}, CSS_LOADER, {
        options: assign({} , CSS_LOADER.options, {
          modules: false,
        }),
      }),
      POSTCSS_LOADER,
    ],
  },
  {
    test: /\.(woff|woff2)$/,
    include: APP_INCLUDEPATHS,
    use: [
      'url-loader',
    ],
  },
];

module.exports = cherrypack({
  development: {
    rules: [
      // {
      //   test: /\.jsx?$/,
      //   enforce: 'pre',
      //   include: includePath,
      //   use: [
      //     'eslint-loader',
      //   ],
      // },
      ...baseRules,
    ],
  },
  production: {
    rules: baseRules,
  },
});
