/**
 * Style Sheet for the {@link Layout} component.
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const layoutStyleSheet = theme => ({
  root: {
    display: 'grid',
    gridTemplateColumns: 'min-content 1fr',
    gridTemplateRows: 'min-content 1fr',
    gridTemplateAreas: [
      '"appbar appbar"',
      '"drawer content"',
    ].join(' '),
    height: '100%',
    background: theme.mintlab.greyscale.lighter,
  },
  appBar: {
    gridArea: 'appbar',
  },
  permanentDrawer: {
    gridArea: 'drawer',
  },
  content: {
    gridArea: 'content',
    overflow: 'auto',
  },
});
