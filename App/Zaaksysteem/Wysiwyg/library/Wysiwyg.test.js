import React from 'react';
import { shallow } from 'enzyme';
import { Wysiwyg } from './Wysiwyg';
import { createEvent } from '../../../test';

/**
 * Editor state dummy.
 * @type {Object}
 */
const editorState = {
  getCurrentContent() {
    return {
      getBlockMap() {
        return {
          forEach() {},
        };
      },
    };
  },
};

/**
 * @test {Wysiwyg}
 */
describe('The `Wysiwyg` component', () => {
  describe('has a `handleChange` method', () => {
    test('that calls the `handleChange` prop', () => {
      const spy = jest.fn();
      const wrapper = shallow(
        <Wysiwyg
          classes={{}}
          name='richtext'
          onChange={spy}
        />
      );
      const instance = wrapper.instance();
      const assertedCallCount = 1;

      instance.handleChange(editorState);

      expect(spy).toHaveBeenCalledTimes(assertedCallCount);
    });
  });

  describe('has a `handleBlur` method', () => {
    test('that stops event propagation', () => {
      const wrapper = shallow(
        <Wysiwyg
          classes={{}}
          name='richtext'
        />
      );
      const instance = wrapper.instance();
      const event = createEvent('blur');
      const assertedCallCount = 1;

      instance.handleBlur(event);
      expect(event.stopPropagation).toHaveBeenCalledTimes(assertedCallCount);
    });

    test('that calls the `onBlur` prop', () => {
      const spy = jest.fn();
      const wrapper = shallow(
        <Wysiwyg
          classes={{}}
          name='richtext'
          onBlur={spy}
        />
      );
      const instance = wrapper.instance();
      const event = createEvent();
      const assertedCallCount = 1;

      instance.handleBlur(event);
      expect(spy).toHaveBeenCalledTimes(assertedCallCount);
    });
  });

  describe('has a `handleFocus` method', () => {
    test('that stops event propagation', () => {
      const wrapper = shallow(
        <Wysiwyg
          classes={{}}
          name='richtext'
        />
      );
      const instance = wrapper.instance();
      const event = createEvent('focus');
      const assertedCallCount = 1;

      instance.handleFocus(event);

      expect(event.stopPropagation).toHaveBeenCalledTimes(assertedCallCount);
    });

    test('that calls the `onFocus` prop', () => {
      const spy = jest.fn();
      const wrapper = shallow(
        <Wysiwyg
          classes={{}}
          name='richtext'
          onFocus={spy}
        />
      );
      const instance = wrapper.instance();
      const event = createEvent('focus');
      const assertedCallCount = 1;

      instance.handleFocus(event);
      expect(spy).toHaveBeenCalledTimes(assertedCallCount);
    });
  });
});
