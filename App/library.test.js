import {
  callOrNothingAtAll,
  cloneWithout,
  equals,
  traverseMap,
  unique,
} from './library';

describe('The library module', () => {
  xdescribe('exports a `unique` function', () => {
    test('that generates unique values', () =>  {
      const a = unique();
      const b = unique();

      const actual = (a !== b);
      const expected = true;

      expect(actual).toBe(expected);
    });

    test('that can be prefixed', () =>  {
      const prefix = 'FOO';

      const actual = unique(prefix).startsWith(prefix);
      const expected = true;

      expect(actual).toBe(expected);
    });
  });

  describe('exports a `cloneWithout` function', () => {
    test('that can clone an object with one excluded property', () => {
      const original = {
        foo: 'foo',
        bar: 'bar',
      };
      const actual = cloneWithout(original, 'foo');
      const expected = {
        bar: 'bar',
      };

      expect(actual).toEqual(expected);
    });

    test('that can clone an object with multiple excluded properties', () => {
      const original = {
        foo: 'foo',
        bar: 'bar',
        quux: 'quux',
      };
      const actual = cloneWithout(original, 'foo', 'quux');
      const expected = {
        bar: 'bar',
      };

      expect(actual).toEqual(expected);
    });
  });

  describe('exports an `equals` function', () => {
    test('that deeply compares values that can be serialized as JSON', () => {
      const actual = {
        foo: 'bar',
        baz: 42,
      };
      const expected = {
        foo: 'bar',
        baz: 42,
      };

      expect(equals(actual, expected)).toBe(true);
    });
  });

  describe('exports a `traverseMap` function', () => {
    test('that returns the output of first value function where the key function is truthy', () => {
      const map = new Map([
        [() => false, () => false],
        [(first, second) => second === 'b', (third, fourth) => fourth],
      ]);

      const keyArgs = ['a', 'b'];
      const functionArgs = ['c', 'd'];
      const actual = traverseMap(map, keyArgs, functionArgs);
      const [, expected] = functionArgs;

      expect(equals(actual, expected)).toBe(true);
    });
  });

  describe('exports a `callOrNothingAtAll` function that', () => {
    test('returns `undefined` if no function is provided', () => {
      const actual = callOrNothingAtAll();
      const expected = undefined;

      expect(actual).toBe(expected);
    });

    test('returns the return value of the provided function', () => {
      const ANSWER = 42;
      const getAnswer = () => ANSWER;
      const actual = callOrNothingAtAll(getAnswer);
      const expected = ANSWER;

      expect(actual).toBe(expected);
    });

    test('calls the provided function with the return value of the provided arguments getter', () => {
      const ANSWER = 42;
      const getAnswer = answer => answer;
      const getArguments = () => [ANSWER];
      const actual = callOrNothingAtAll(getAnswer, getArguments);
      const expected = ANSWER;

      expect(actual).toBe(expected);
    });
  });

});
