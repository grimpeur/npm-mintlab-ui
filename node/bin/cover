#!/usr/bin/env node
const { readFileSync, writeFileSync } = require('fs');
const { join, resolve } = require('path');
const MarkdownIt = require('markdown-it');

const ENCODING = 'utf8';
const sourceFile = resolve('README.md');
const { npm_package_config_PUBLIC } = process.env;
const DEFAULT_ROOT = 'public';

if (npm_package_config_PUBLIC !== DEFAULT_ROOT) {
  const dummyFile = resolve(DEFAULT_ROOT, 'index.html');
  const localRoot = npm_package_config_PUBLIC
    .split('/')
    .slice(1)
    .join('/');
  const dummyTemplate = `<script>window.location.replace('/${localRoot}/')</script>`.trim();

  writeFileSync(dummyFile, dummyTemplate);
}

const outputFile = resolve(npm_package_config_PUBLIC, 'index.html');
const markdownIt = new MarkdownIt();
const readme = readFileSync(sourceFile, ENCODING);
const markdown = markdownIt.render(readme);

const styleSheet = readFileSync(join(__dirname, 'library', 'cover.css'), ENCODING);
const logo = readFileSync(join(__dirname, 'library', 'logo.svg'), ENCODING);

const template = `
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>@mintlab/ui</title>
<style>${styleSheet}</style>
</head>
<body>
  <header>${logo}</header>

  <nav>
    <h2><code>GOTO</code></h2>
    <ul>
      <li><a href="./storybook/">Storybook</a></li>
      <li><a href="./documentation/consumer">Consumer Documentation</a></li>
      <li><a href="./documentation/developer">Developer Documentation</a></li>
      <li><a href="./coverage/lcov-report/">Test Coverage</a></li>
      <li><a href="./webpack/visualizer.html">Webpack Visualizer</a></li>
    </ul>
  </nav>

  <main>${markdown}</main>
</body>
</html>
`.trim();

writeFileSync(outputFile, template);
