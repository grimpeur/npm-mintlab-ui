/**
 * Generates classnames through the withStyles HOC.
 * This gets injected as the `classes` prop into the Tooltip component
 *
 * @param {Object} theme
 */
export const tooltipStyleSheet = ({
  palette: {
    primary,
    common,
    error,
  },
  mintlab: {
    radius,
  },
}) => ({
  all: {
    padding: '8px',
    maxWidth: '300px',
    borderRadius: radius.toolTip,
  },
  popper: {
    opacity: '1',
  },
  default: {
    backgroundColor: common.black,
  },
  error: {
    backgroundColor: error.dark,
  },
  info: {
    backgroundColor: primary.light,
    color: primary.main,
  },
});
