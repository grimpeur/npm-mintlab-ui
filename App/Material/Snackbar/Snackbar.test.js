import React from 'react';
import { shallow } from 'enzyme';
import Snackbar from '.';

/**
 * @test {Snackbar}
 */
describe('The `Snackbar` component', () => {
  describe('has a `componentDidUpdate` lifecycle method', () => {
    test('pushes new props to the state queue', () => {
      const wrapper = shallow(
        <Snackbar/>
      );
      const instance = wrapper.instance();

      instance.componentDidUpdate({
        someUpdate: true,
      });

      const actual = instance.state.queue.length;
      const asserted = 1;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `handleClose` method', () => {
    test('that does not set `state.open` if the event reason is `clickaway`', () => {
      const wrapper = shallow(
        <Snackbar/>
      );
      const instance = wrapper.instance();

      instance.handleClose(null, 'clickaway');

      const actual = instance.state.open;
      const asserted = true;

      expect(actual).toBe(asserted);
    });

    test('that sets `state.open` if the event reason is not `clickaway`', () => {
      const wrapper = shallow(
        <Snackbar/>
      );
      const instance = wrapper.instance();

      instance.handleClose(null, 'Hello, sailor!');

      const actual = instance.state.open;
      const asserted = false;

      expect(actual).toBe(asserted);
    });
  });

  describe('has a `processQueue` method', () => {
    test('that calls `onQueueEmpty` if the queue is empty', () => {
      const spy = jest.fn();
      const wrapper = shallow(
        <Snackbar
          onQueueEmpty={spy}
        />
      );
      const instance = wrapper.instance();
      const assertedTimesCalled = 1;

      instance.processQueue();
      expect(spy).toHaveBeenCalledTimes(assertedTimesCalled);
    });
  });

  test('has a `getCloneWithout` method that passes filtered props to the Material-UI component', () => {
    const wrapper = shallow(
      <Snackbar/>
    );
    const instance = wrapper.instance();
    const props = {
      onQueueEmpty: '',
      onClose: '',
      onExited: '',
      open: '',
      action: '',
      foo: 'bar',
    };

    const actual = instance.getCloneWithout(props);
    const asserted = {
      foo: 'bar',
    };

    expect(actual).toEqual(asserted);
  });
});
