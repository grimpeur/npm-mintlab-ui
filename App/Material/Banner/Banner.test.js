import React from 'react';
import { shallow } from 'enzyme';
import Banner from '.';

/**
 * @test {Banner}
 */
describe('The `Banner` component', () => {
  test('displays its children as a message', () => {
    const wrapper = shallow(
      <Banner>
        Hello, world!
      </Banner>
    );
    const actual = wrapper.text();
    const expected = 'Hello, world!';

    expect(actual).toBe(expected);
  });
});
