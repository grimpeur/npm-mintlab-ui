ARG WORKSPACE=/home/node/ui

#### Stage: update npm and create working directory
FROM node:carbon-alpine AS base
ARG WORKSPACE
RUN npm install -g npm@6.4.1\
 && mkdir $WORKSPACE\
 && chown -R node:node $WORKSPACE

#### Stage: copy files
FROM base AS files
ARG WORKSPACE
WORKDIR $WORKSPACE
# copy dot and package files to root
COPY --chown=node:node ./root/* ./
# The README is used to generate the documentation cover page
COPY --chown=node:node ./README.md ./README.md
COPY --chown=node:node ./App ./App
COPY --chown=node:node ./node ./node
COPY --chown=node:node ./root ./root

#### Stage: install dependencies
FROM files AS install
USER node
RUN npm install --no-optional --loglevel error

#### Stage: build distribution
FROM install AS distribution
RUN npm run lint\
 && npm run test\
 && npm run webpack

#### Stage: set up development environment
FROM distribution AS development
RUN rm ./package*.json\
 && ln -s ./root/package.json ./package.json\
 && ln -s ./root/package-lock.json ./package-lock.json
