import {
  React,
  stories,
  text,
} from '../../story';
import Paper from '.';

stories(module, __dirname, {
  Default() {
    return (
      <Paper>
        {text('Greeting', 'Hello, world!')}
      </Paper>
    );
  },
});
