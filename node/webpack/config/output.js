const { join } = require('path');

module.exports = {
  chunkFilename: '[name].js',
  filename: '[name].js',
  libraryTarget: 'commonjs2',
  path: join(process.cwd(), 'distribution'),
};
