const BORDER_RADIUS_MULTIPLIER = 3;

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const statusBarStyleSheet = ({
  palette: {
    text,
    primary,
    error,
  },
  shape: {
    borderRadius,
  },
  typography,
}) => ({
  bar: {
    borderRadius: borderRadius * BORDER_RADIUS_MULTIPLIER,
    display: 'flex',
    'align-items': 'center',
    padding: '12px',
  },
  disabled: {
    backgroundColor: error.light,
  },
  text: {
    marginRight: '14px',
    fontWeight: typography.fontWeightMedium,
  },
  error: {
    color: text.secondary,
  },
  info: {
    color: primary.main,
  },
});
