# Usage

## Imports

### CSS

    import '@mintlab/ui/distribution/ui.css';

### React Components

    import { Resource } from '@mintlab/ui';

[API documentation](../identifiers.html)
