import React from 'react';
import MuiAppBar from '@material-ui/core/AppBar';
import MuiToolbar from '@material-ui/core/Toolbar';
import Divider from '@material-ui/core/Divider';
import Button from '../../../Material/Button';
import DropdownMenu, { DropdownMenuList } from '../../DropdownMenu';
import { Subtitle2 } from '../../../Material/Typography';
import { appBarStyleSheet } from './AppBar.style';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

/**
 * *Material Design* **AppBar**.
 * - facade for *Material-UI* `AppBar` and `ToolBar`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/AppBar
 * @see /npm-mintlab-ui/documentation/consumer/manual/AppBar.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {*} props.children
 * @param {string} props.className
 * @param {number} props.gutter
 *   Horizontal icon button gutter
 * @param {string} props.menuLabel
 * @param {Function} props.onMenuClick
 * @param {string} props.userLabel
 * @param {Array} props.userActions
 * @return {ReactElement}
 */
export const AppBar = ({
  classes,
  children,
  className,
  gutter,
  menuLabel,
  onMenuClick,
  userLabel,
  userName,
  userActions,
}) => (
  <MuiAppBar
    className={className}
    color="inherit"
    position="static"
    classes={{
      root: classNames(className, classes.appBar),
    }}
  >
    <MuiToolbar
      disableGutters={true}
      classes={{
        root: classes.toolbar,
      }}
    >
      <div
        style={{
          padding: `0 ${gutter}px`,
        }}
      >
        <Button
          action={onMenuClick}
          label={menuLabel}
          presets={['icon', 'small']}
        >menu</Button>
      </div>
      <div className={classes.content}>
        <Subtitle2>
          {children}
        </Subtitle2>
      </div>
      <div
        style={{
          padding: `0 ${gutter}px`,
        }}
      >
        <DropdownMenu
          trigger={
            <Button
              label={userLabel}
              presets={['icon', 'small']}
            >face
            </Button>}
        >
          <Subtitle2
            classes={{
              root: classes.userName,
            }}>{userName}</Subtitle2>
          <Divider />
          <DropdownMenuList
            items={userActions}
          />
        </DropdownMenu>
      </div>
    </MuiToolbar>
  </MuiAppBar>
);

export default withStyles(appBarStyleSheet)(AppBar);
