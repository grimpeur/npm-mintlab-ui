# 🔌 `Banner` component

> *Material Design* **Banner**.

## See also

- [`Banner` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Banner)
- [`Banner` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-banner)

## External resources

- [*Material Guidelines*: Banners](https://material.io/design/components/banners.html)
