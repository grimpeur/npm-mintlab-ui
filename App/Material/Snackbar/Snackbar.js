import React, { Component } from 'react';
import MuiSnackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import Icon from '../Icon/Icon';
import { bind, equals, cloneWithout } from '../../library';

/**
 * *Material Design* **Snackbar**.
 * - facade for *Material-UI* `Snackbar`
 * - all props but `onQueueEmpty`, `onClose`, `onExited`, `open` and `action`
 *   are spread to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Snackbar
 * @see /npm-mintlab-ui/documentation/consumer/manual/Snackbar.html
 * @see https://material-ui.com/api/snackbar/
 *
 * @reactProps {string} message
 * @reactProps {Function} onQueueEmpty
 */
export class Snackbar extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      open: false,
      autoHideDuration: 6000,
      key: new Date().getTime(),
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.state = {
      currentMessage: {},
      queue: [props],
    };
    bind(this, 'handleClose', 'handleExited');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    this.processQueue();
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidupdate
   *
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    if (!equals(this.props, prevProps)) {
      const {
        state: {
          queue,
          open,
        },
      } = this;

      // ZS-FIXME: See FIXME in `processQueue`
      queue.push(this.props);
      this.setState({ queue });

      if (open) {
        this.setState({ open: false });
      } else {
        this.processQueue();
      }
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      state: {
        open,
        currentMessage,
      },
      handleClose,
      handleExited,
      getCloneWithout,
    } = this;

    return (
      <MuiSnackbar
        onClose={handleClose}
        onExited={handleExited}
        open={open}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            className="close"
            onClick={handleClose}
          >
            <Icon>close</Icon>
          </IconButton>,
        ]}
        {...getCloneWithout(currentMessage)}
      />
    );
  }

  // Custom methods:

  /**
   * @param event
   * @param {string} reason
   */
  handleClose(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({ open: false });
  }

  handleExited() {
    this.processQueue();
  }

  processQueue() {
    const {
      state: {
        queue,
      },
      props: {
        onQueueEmpty,
      },
    } = this;

    if (queue.length) {
      // ZS-FIXME: mutating state directly, even before calling `setState`, is bad practice.
      // For an example, see https://daveceddia.com/why-not-modify-react-state-directly/
      const currentMessage = queue.shift();

      this.setState({
        currentMessage,
        queue,
        open: true,
      });
    } else if (onQueueEmpty) {
      onQueueEmpty();
    }
  }

  /**
   * @param {Object} props
   * @return {Object}
   */
  getCloneWithout(props) {
    return cloneWithout(props, 'onQueueEmpty', 'onClose', 'onExited', 'open', 'action');
  }
}

export default Snackbar;
